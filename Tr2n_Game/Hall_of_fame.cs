﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Tr2n_Game
{
    public class RecordClass
    {
        public int time;
        public string name;
        public int position = 0;

        public RecordClass(int time, string UserName)
        {
            this.time = time;
            this.name = UserName;
        }
    }

    public class Hall_of_fame
    {
        string path = "Hall_of_fame.txt";

        public void WriteScore(string ExportString, int TimeResult)
        {
            StreamWriter SW = new StreamWriter(path, true);
            ExportString += " ";
            ExportString += TimeResult.ToString();
            SW.WriteLine(ExportString);
            path = Path.GetFullPath(path);
            SW.Close();
        }

        public List<RecordClass> ReadScore()
        {
            StreamReader SR = new StreamReader(path); List<RecordClass> res = new List<RecordClass>(); while (true)
            {
                string temp = SR.ReadLine(); if (temp == null) { break; }
                string pattern = @"(\w+)\s(\d+)"; 
                foreach (Match m in Regex.Matches(temp, pattern))
                {
                    int value1 = Int32.Parse(m.Groups[2].Value); 
                    string value2 = m.Groups[1].Value.ToString();
                    res.Add(new RecordClass(value1, value2));
                }
            }
            SR.Close(); return res;
        }
    }
}