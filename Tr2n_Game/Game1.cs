﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Tr2n_Game {

    public class Game1 : Microsoft.Xna.Framework.Game {
        
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        SpriteFont MyFontNameTime;
        SpriteFont MyFontText;

        int is_MouseLB_Pressed = 5;

        //Фоновая музыка
        Song grid;
        Song level1;
        Song level2;
        Song about;
        //Звук победы
        Song vict;

        //Звук поражения
        Song def;

        //Звук удара персонажа
        SoundEffect sam_strike;

        //Звук удара противника
        SoundEffect guard_strike;

        //Звук смерти противника
        SoundEffect death;

        //Звук попадания удара в цель
        SoundEffect hurt_sound;

        //Звук взаимодействия
        SoundEffect get_item;
        
        
        //Для записи времени 
        string TimeFirst = "";
        Vector2 TimeFirstPos = new Vector2(400, 10);
        float timer;
        int timerEnd;
        int timer1;
        int timer2;

        //Menu
        Texture2D mainMenuTexture;
        Vector2 mainMenuPosition;
        bool mainMenu = false;

        //Ввод имени пользователя
        Texture2D nameTexture;
        Vector2 namePosition;
        bool name = true;
        string UserName = "";
        Vector2 userNamePosition;
        int num = 0;
        int entername = 20;

        //Уровень сложности уровня
        Texture2D difficultyTexture;
        Vector2 difficultyPosition;
        bool difficulty = false;

        //Таблица результатоы
        Texture2D resultsTexture;
        Vector2 resultsPosition;
        bool results = false;
        Hall_of_fame MyScore = new Hall_of_fame();
        List<RecordClass> ScoreStr = new List<RecordClass>();
        bool writeResults = false;

        //Успешное завершение уровня
        Texture2D victoryTexture;
        Vector2 victoryPosition;
        bool victory = false;

        //Провал во время прохождения уровня
        Texture2D defeatTexture;
        Vector2 defeatPosition;
        bool defeat = false;

        //Уровни
        bool first_level_simple = false;
        bool first_level_difficult = false;
        bool second_level_simple = false;
        bool second_level_difficult = false;

        Rectangle Level = new Rectangle(100, 286, 450, 90);
        Rectangle BeginnerLevel = new Rectangle(106, 371, 250, 90);
        Rectangle DifficultLevel = new Rectangle(690, 371, 300, 70);
        Rectangle Back = new Rectangle(100, 816, 150, 100);
        Rectangle Hall_of_fame = new Rectangle(100, 499, 450, 80);
        Rectangle Exit1 = new Rectangle(100, 726, 230, 60);
        

        //Прередвижение второго типа врагов
        bool upEnemy = false;
        bool downEnemy = true;

        int energy_hit = 500;

        bool loading = false;

        string Tip = "";
        string text = "";

        Vector2 posTip = new Vector2(10, 10);
        Vector2 posText = new Vector2(10, 70);

        Texture2D Container_key;
        Texture2D Gate2_key;
        Texture2D Gate1_key;
        Texture2D Health;

        Vector2 posInSK = new Vector2(770, 3);
        Vector2 posInDK = new Vector2(770, 53);
        Vector2 posInLK = new Vector2(830, 3);
        Vector2 posInLR = new Vector2(970, 3);
        Vector2 posInER = new Vector2(970, 50);
        Vector2 posInTextSK = new Vector2(790, 27);
        Vector2 posInTextDK = new Vector2(790, 77);
        Vector2 posInTextLK = new Vector2(850, 27);

        string InvSK = "";
        string InvDK = "";
        string InvLK = "";


        double elapsedTime = 0;
        double elapsedTwoTime = 0;
        double totalTime = 0;
        double timeText = 0;

        Sam_Flynn player;

        Texture2D Grid_Map;
        Vector2 position_map_first = new Vector2(0, 0);

        Texture2D panel;
        Vector2 posPanel = new Vector2(0, 0);


        Vector2 posKey_door = new Vector2(988, 720);

        int lifes;
        int isPressed = 0;

        const int frames = 5;
        const int Gr1 = 3;
        const int Gr2 = 4;

        //Частота кадров в секунду для анимации спрайта
        const int framesPerSec = 10;
        const int GuardPerSec = 6;
        const int Guard2PerSec = 8;

        //Текущий кадр для вывода на экран
        int numberOfFrame = 0;
        int numberGuard = 0;
        int nubmerGuard2 = 0;

        //Время, в течение которого отображается кадр
        float timePerFrame;
        float timeGr;
        float timeGr2;

        //Прошедшее время после первого кадра спрайта
        float totalElapsed;
        float totalElapsedd;
        float totalElapseddd;

        //Позиция для вывода спрайта
        Rectangle sprRec;
        Rectangle Rec;
        Rectangle Recc;

        int widthFrame = 110;

        int countSilver_keys = 0;
        int countKey_door = 0;
        int countKey_lattice = 0;

        bool[] activ_key = new bool[4];

        public List<int> Guard1_Health = new List<int>();
        public List<int> Guard2_Health = new List<int>();
        public List<bool> Container_open = new List<bool>();
        public List<Container> Container = new List<Container>();
        public List<Key_one> key = new List<Key_one>();
        public List<Wall> wall = new List<Wall>();
        public List<Health> Health_ = new List<Health>();
        public List<Gate2> Gate1 = new List<Gate2>();
        public List<Sam_attack> Player_Attack = new List<Sam_attack>();
        public List<Vertigo_Wall> Vertigo_Wall = new List<Vertigo_Wall>();
        public List<Gate1> Gate_1 = new List<Gate1>();
        public List<Guard> Guard1 = new List<Guard>();
        public List<Guard_attack> Guard1_attack = new List<Guard_attack>();
        public List<Guard_Health> Guard1_health = new List<Guard_Health>();
        public List<Gate2_key> Gate2_K = new List<Gate2_key>();
        public List<Guard2> Guard_2 = new List<Guard2>();
        public List<Guard2_Health> G2_Health = new List<Guard2_Health>();
        public List<Guard2_attack> G2_Attack = new List<Guard2_attack>();

        public int[,] Layer;

        Vector2 new_position = new Vector2();
        public Game1() {
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferHeight = 1000;
            graphics.PreferredBackBufferWidth = 1100;
            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;

        }

        protected override void Initialize() {
            base.Initialize();
        }

        protected override void LoadContent() {

            //Внитриигровая музыка
            grid = Content.Load<Song>("grid");
            level1 = Content.Load<Song>("level1");
            level2 = Content.Load<Song>("level2");
            about = Content.Load<Song>("about");
            MediaPlayer.Play(grid);
            MediaPlayer.IsRepeating = true;
            vict = Content.Load<Song>("victory1");
            def = Content.Load<Song>("defeat1");

            //Внитриигровые звуковые эффекты
            sam_strike = Content.Load<SoundEffect>("sam_strike");
            guard_strike = Content.Load<SoundEffect>("guard_strike");
            death = Content.Load<SoundEffect>("derezzed");
            hurt_sound = Content.Load<SoundEffect>("hurt");
            get_item = Content.Load<SoundEffect>("get");

            spriteBatch = new SpriteBatch(GraphicsDevice);

            //Шрифты для отображения результатов пользователей и таймера
            MyFontText = Content.Load<SpriteFont>("Times New Roman");
            MyFontNameTime = Content.Load<SpriteFont>("Courier New");
            
            spriteBatch = new SpriteBatch(GraphicsDevice);
            //Текстуры и их координаты
            player = new Sam_Flynn(Content.Load<Texture2D>("Sam_Flynn"), new Vector2(110f, 800f));
            panel = Content.Load<Texture2D>("upper_panel");
            Grid_Map = Content.Load<Texture2D>("Back_map");

            mainMenuTexture = Content.Load<Texture2D>("Menu");
            mainMenuPosition = new Vector2(0f, 0f);
            
            difficultyTexture = Content.Load<Texture2D>("DiffChoice");
            difficultyPosition = new Vector2(0f, 0f);

            defeatTexture = Content.Load<Texture2D>("Defeat");
            defeatPosition = new Vector2(0f, 0f);

            victoryTexture = Content.Load<Texture2D>("Victory");
            victoryPosition = new Vector2(0f, 0f);

            resultsTexture = Content.Load<Texture2D>("Hall_of_fame");
            resultsPosition = new Vector2(0f, 0f);

            nameTexture = Content.Load<Texture2D>("Auth");
            namePosition = new Vector2(0f, 0f);
            userNamePosition = new Vector2(200f, 400f);

        }

        protected override void UnloadContent() {

            spriteBatch.Dispose();
        }
        void ChangeFrame(float elpT) {
            //Увеличим общее время отображения спрайта на время, прошедшее с последнего вызова процедуры             
            totalElapsed += elpT;
            //если общее время отображения спрайта больше времени,             
            //выделенного на один спрайт               
            if (totalElapsed > timePerFrame) {
                //Если номер кадра = количество кадров-1                     
                if (numberOfFrame == frames - 1) {
                    //То установим номер кадра равным нулю                     
                    numberOfFrame = 0;
                }
                else numberOfFrame++;                    
                //Его координата X соответствует координате левого верхнего угла                     
                //кадра, Y равно 0, длина и ширина всегда равны 110 и 100                     
                sprRec = new Rectangle((int)widthFrame * numberOfFrame, 0, 110, 100);

                //сбросим totalElapsed в 0                     
                totalElapsed = 0;
            }
        }

        void AnimationGuard1(float elpT) {
            //Увеличим общее время отображения спрайта на время, прошедшее с последнего вызова процедуры             
            totalElapsedd += elpT;
            //Если общее время отображения спрайта больше времени, выделенного на один спрайт...    
            if (totalElapsedd > timeGr) {
                if (numberGuard == Gr1 - 1) {
                    numberGuard = 0;
                }
                else numberGuard++;

                Rec = new Rectangle((int)widthFrame * numberGuard, 0, 108, 98);
                //..., то сбросим totalElapsedd в 0                     
                totalElapsedd = 0;
            }
        }

        void AnimationGuard2(float elpT) {  
            totalElapseddd += elpT;
            if (totalElapseddd > timeGr2) {
                if (nubmerGuard2 == Gr2 - 1)
                    nubmerGuard2 = 0;
                else nubmerGuard2++;
                Recc = new Rectangle((int)widthFrame * nubmerGuard2, 0, 110, 100);                  
                totalElapseddd = 0;
            }
        }

        void unloading() {
            for (int i = 0; i < Health_.Count;) {
                Health_.RemoveAt(i);
            }
            for (int i = 0; i < Guard1_Health.Count;) {
                Guard1_Health.RemoveAt(i);
            }
            for (int i = 0; i < Guard2_Health.Count;) {
                Guard2_Health.RemoveAt(i);
            }
            for (int i = 0; i < Container_open.Count;) {
                Container_open.RemoveAt(i);
            }
            for (int i = 0; i < Container.Count;) {
                Container.RemoveAt(i);
            }
            for (int i = 0; i < key.Count;) {
                key.RemoveAt(i);
            }
            for (int i = 0; i < wall.Count; ) {
                wall.RemoveAt(i);
            }
            for (int i = 0; i < Gate1.Count;) {
                Gate1.RemoveAt(i);
            }
            for (int i = 0; i < Player_Attack.Count;) {
                Player_Attack.RemoveAt(i);
            }
            for (int i = 0; i < Guard1.Count;) {
                Guard1.RemoveAt(i);
            }
            for (int i = 0; i < Guard1_attack.Count;) {
                Guard1_attack.RemoveAt(i);
            }
            for (int i = 0; i < Vertigo_Wall.Count;) {
                Vertigo_Wall.RemoveAt(i);
            }
            for (int i = 0; i < Gate_1.Count;) {
                Gate_1.RemoveAt(i);
            }
            for (int i = 0; i < Guard1_health.Count;) {
                Guard1_health.RemoveAt(i);
            }
            for (int i = 0; i < Gate2_K.Count;) {
                Gate2_K.RemoveAt(i);
            }
            for (int i = 0; i < Guard_2.Count;) {
                Guard_2.RemoveAt(i);
            }
            for (int i = 0; i < G2_Health.Count;) {
                G2_Health.RemoveAt(i);
            }
            for (int i = 0; i < G2_Attack.Count;) {
                G2_Attack.RemoveAt(i);
            }
        }

        void EnterName() {
            KeyboardState kbState = Keyboard.GetState();
            entername -= 1;
            if (entername <= 0 && num <= 15) {
                if (kbState.IsKeyDown(Keys.A)) {
                    UserName += "A";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.B)) {
                    UserName += "B";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.C)) {
                    UserName += "C";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.D)) {
                    UserName += "D";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.E)) {
                    UserName += "E";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.F)) {
                    UserName += "F";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.G)) {
                    UserName += "G";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.H)) {
                    UserName += "H";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.I)) {
                    UserName += "I";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.J)) {
                    UserName += "J";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.K)) {
                    UserName += "K";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.L)) {
                    UserName += "L";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.N)) {
                    UserName += "N";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.O)) {
                    UserName += "O";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.P)) {
                    UserName += "P";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.Q)) {
                    UserName += "Q";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.R)) {
                    UserName += "R";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.S)) {
                    UserName += "S";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.T)) {
                    UserName += "T";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.U)) {
                    UserName += "U";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.V)) {
                    UserName += "V";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.W)) {
                    UserName += "W";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.X)) {
                    UserName += "X";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.Y)) {
                    UserName += "Y";
                    num++;
                    entername = 20;
                }
                if (kbState.IsKeyDown(Keys.Z)) {
                    UserName += "Z";
                    num++;
                    entername = 20;
                }
            }
        }

        public void load() {
            if ((first_level_simple || first_level_difficult || second_level_simple || second_level_difficult)) {

                writeResults = true;
                timer = 0;
                countKey_door = 0;
                countKey_lattice = 0;
                countSilver_keys = 0;
                player.spPosition.X = 110;
                player.spPosition.Y = 800;
                lifes = 500;
                //Время для одного кадра вычисляется как результат деления 1 секунды на заданное количество кадров в секунду
                timePerFrame = (float)1 / framesPerSec;
                timeGr = (float)1 / GuardPerSec;
                timeGr2 = (float)1 / Guard2PerSec;
                sprRec = new Rectangle(0, 0, 110, 100);

                if (first_level_simple) {
                    Layer = new int[10, 10] {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                { 0, 0, 0, 3, 4, 0, 3, 0, 0, 0},
                { 0, 0, 0, 3, 0, 0, 3, 0, 0, 0},
                { 0, 1, 0, 3, 0, 0, 3, 0, 0, 0},
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                { 0, 0, 0, 0, 6, 6, 6, 7, 6, 0},
                { 0, 0, 0, 0, 3, 0, 3, 0, 0, 0},
                { 6, 6, 6, 0, 0, 4, 3, 0, 4, 0},
                { 0, 0, 0, 0, 0, 0, 3, 8, 0, 0},
                { 0, 2, 0, 0, 5, 0, 3, 9, 0, 0}
                };  MediaPlayer.Stop();
                    MediaPlayer.Play(level1);
                    MediaPlayer.IsRepeating = true;
                }
                else if (first_level_difficult) {
                    Layer = new int[10, 10] {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                { 0, 0, 0, 3, 4, 0, 3, 0, 0, 0},
                { 0, 0, 0, 3, 0, 0, 3, 0, 0, 0},
                { 0, 1, 0, 3, 0, 0, 3, 0, 0, 0},
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                { 0, 0, 0, 0, 6, 6, 6, 7, 6, 0},
                { 0, 0, 0, 0, 3, 0, 3, 0, 0, 0},
                { 6, 6, 6, 8, 0, 4, 3, 8, 4, 0},
                { 0, 0, 0, 0, 0, 0, 3, 0, 0, 0},
                { 0, 2, 0, 0, 5, 0, 3, 9, 0, 0}
                };  MediaPlayer.Stop();
                    MediaPlayer.Play(level1);
                    MediaPlayer.IsRepeating = true;
                }
                else if (second_level_simple) {
                    Layer = new int[10, 10] {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                { 0, 0, 4, 0, 4, 0, 4, 0, 0, 0},
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                { 0, 1, 0, 0, 0, 0, 0, 0, 2, 0},
                { 0, 6, 6, 6, 7, 6, 6, 6, 6, 0},
                { 0, 0, 0, 3, 0, 0, 0, 0, 0, 0},
                { 0, 0, 1, 3, 10, 0, 6, 7, 6, 0},
                { 0, 0, 0, 3, 0, 4, 3, 0, 0, 0},
                { 0, 0, 0, 0, 0, 0, 3, 8, 0, 0},
                { 0, 2, 0, 0, 5, 0, 3, 9, 0, 0}
                };  MediaPlayer.Stop();
                    MediaPlayer.Play(level2);
                    MediaPlayer.IsRepeating = true;
                }
                else if (second_level_difficult) {
                    Layer = new int[10, 10] {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                { 0, 0, 4, 0, 4, 0, 4, 0, 4, 0},
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                { 0, 1, 10, 0, 0, 0, 0, 0, 2, 0},
                { 0, 6, 6, 6, 7, 6, 6, 6, 6, 0},
                { 0, 0, 0, 3, 0, 0, 0, 0, 0, 0},
                { 0, 0, 1, 3, 10, 0, 6, 7, 6, 0},
                { 0, 0, 0, 3, 8, 4, 3, 0, 0, 0},
                { 0, 0, 0, 0, 0, 0, 3, 8, 0, 0},
                { 0, 2, 0, 0, 5, 0, 3, 9, 0, 0}
                };
                    MediaPlayer.Stop();
                    MediaPlayer.Play(level2);
                    MediaPlayer.IsRepeating = true;
                }

                AddObjFirstLevelSimple();
                Container_key = Content.Load<Texture2D>("Container_key");
                Gate2_key = Content.Load<Texture2D>("Gate2_key");
                Gate1_key = Content.Load<Texture2D>("Gate_key");
                Health = Content.Load<Texture2D>("Life0");
            }
        }


        void AddObjFirstLevelSimple() {
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    if (Layer[i, j] == 1) {
                        Container_open.Add(true);
                        Container.Add(new Container(Content.Load<Texture2D>("Container"), new Vector2(i, j)));
                    }
                    if (Layer[i, j] == 2)
                        key.Add(new Key_one(Content.Load<Texture2D>("Key"), new Vector2(i, j)));
                    if (Layer[i, j] == 3)
                        wall.Add(new Wall(Content.Load<Texture2D>("Wall"), new Vector2(i, j)));
                    if (Layer[i, j] == 4)
                        Health_.Add(new Health(Content.Load<Texture2D>("Life_item"), new Vector2(i, j)));
                    if (Layer[i, j] == 5)
                        Gate1.Add(new Gate2(Content.Load<Texture2D>("Gate2"), new Vector2(i, j)));
                    if (Layer[i, j] == 6)
                        Vertigo_Wall.Add(new Vertigo_Wall(Content.Load<Texture2D>("Wall_Vertigo"), new Vector2(i, j)));
                    if (Layer[i, j] == 7)
                        Gate_1.Add(new Gate1(Content.Load<Texture2D>("Gate"), new Vector2(i, j)));
                    if (Layer[i, j] == 8) {
                        Guard1.Add(new Guard(Content.Load<Texture2D>("Guard"), new Vector2(i, j)));
                        Guard1_health.Add(new Guard_Health(Content.Load<Texture2D>("life_20"), new Vector2(i * 110, j * 100 - 30)));
                        Guard1_Health.Add(1000);
                    }
                    if (Layer[i, j] == 9) {
                        Gate2_K.Add(new Gate2_key(Content.Load<Texture2D>("Gate2_key"), new Vector2(i, j)));
                    }
                    if (Layer[i, j] == 10) {
                        Guard_2.Add(new Guard2(Content.Load<Texture2D>("Guard2"), new Vector2(i, j)));
                        G2_Health.Add(new Guard2_Health(Content.Load<Texture2D>("life_20"), new Vector2(i * 110, j * 100 - 30)));
                        Guard2_Health.Add(2000);
                    }
                }
            }

        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            KeyboardState kbState = Keyboard.GetState();
            MouseState mState = Mouse.GetState();

            new_position.X = Mouse.GetState().X;
            new_position.Y = Mouse.GetState().Y;
            new_position.X = mState.X;
            new_position.Y = mState.Y;

            if (name) {
                EnterName();
                if ((kbState.IsKeyDown(Keys.Enter)) && num > 0) {
                    mainMenu = true;
                    name = false;
                }
            }

            if (mainMenu || difficulty || results) {
                if (mState.LeftButton == ButtonState.Pressed) {
                    is_MouseLB_Pressed -= 1;
                    new_position.X = Mouse.GetState().X;
                    new_position.Y = Mouse.GetState().Y;
                }

                
                if (is_MouseLB_Pressed <= 0) {

                    if (mainMenu) {
                        loading = true;
                        defeat = false;
                        victory = false;
                        
                        if (Level.Contains(mState.X, mState.Y) && mState.LeftButton == ButtonState.Pressed) {
                            mainMenu = false;
                            difficulty = true;
                            is_MouseLB_Pressed = 5;
                        }

                        if (Hall_of_fame.Contains(mState.X, mState.Y) && mState.LeftButton == ButtonState.Pressed) {
                            mainMenu = false;
                            results = true;
                            is_MouseLB_Pressed = 5;
                            MediaPlayer.Stop();
                            MediaPlayer.Play(about);
                            MediaPlayer.IsRepeating = true;
                        }


                        if (Exit1.Contains(mState.X, mState.Y) && mState.LeftButton == ButtonState.Pressed)
                            Exit();
                    }

                    if (difficulty) {
                        if (BeginnerLevel.Contains(mState.X, mState.Y) && mState.LeftButton == ButtonState.Pressed) {
                            if (num > 0) {
                                difficulty = false;
                                is_MouseLB_Pressed = 5;
                                first_level_simple = true;
                                loading = true;
                                name = false;
                            }
                        }

                        if (DifficultLevel.Contains(mState.X, mState.Y) && mState.LeftButton == ButtonState.Pressed) {
                            if (num > 0) {
                                difficulty = false;
                                is_MouseLB_Pressed = 5;
                                first_level_difficult = true;
                                loading = true;
                            }
                        }

                        if (Back.Contains(mState.X, mState.Y) && mState.LeftButton == ButtonState.Pressed) {
                            difficulty = false;
                            mainMenu = true;
                            is_MouseLB_Pressed = 5;
                        }
                    }              

                    if (results) {
                        if (Back.Contains(mState.X, mState.Y) && mState.LeftButton == ButtonState.Pressed) {
                            results = false;
                            mainMenu = true;
                            is_MouseLB_Pressed = 5;
                            MediaPlayer.Stop();
                            MediaPlayer.Play(grid);
                            MediaPlayer.IsRepeating = true;
                        }
                    }
                }

            }

            if (loading) {
                load();
                loading = false;
            }

            if ((first_level_simple || first_level_difficult || second_level_simple || second_level_difficult)) {
                if (!victory) {
                    timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (first_level_simple || first_level_difficult) {
                        timer1 = (int)timer;
                    }
                    if (second_level_simple || second_level_difficult) {
                        timer2 = (int)timer;
                    }
                }


                if (lifes <= 0)
                    Health = Content.Load<Texture2D>("life_0");
                else if (lifes == 25)
                    Health = Content.Load<Texture2D>("life_1");
                else if (lifes == 50)
                    Health = Content.Load<Texture2D>("life_2");
                else if (lifes == 75)
                    Health = Content.Load<Texture2D>("life_3");
                else if (lifes == 100)
                    Health = Content.Load<Texture2D>("life_4");
                else if (lifes == 125)
                    Health = Content.Load<Texture2D>("life_5");
                else if (lifes == 150)
                    Health = Content.Load<Texture2D>("life_6");
                else if (lifes == 175)
                    Health = Content.Load<Texture2D>("life_7");
                else if (lifes == 200)
                    Health = Content.Load<Texture2D>("life_8");
                else if (lifes == 225)
                    Health = Content.Load<Texture2D>("life_9");
                else if (lifes == 250)
                    Health = Content.Load<Texture2D>("life_10");
                else if (lifes == 275)
                    Health = Content.Load<Texture2D>("life_11");
                else if (lifes == 300)
                    Health = Content.Load<Texture2D>("life_12");
                else if (lifes == 325)
                    Health = Content.Load<Texture2D>("life_13");
                else if (lifes == 350)
                    Health = Content.Load<Texture2D>("life_14");
                else if (lifes == 375)
                    Health = Content.Load<Texture2D>("life_15");
                else if (lifes == 400)
                    Health = Content.Load<Texture2D>("life_16");
                else if (lifes == 425)
                    Health = Content.Load<Texture2D>("life_17");
                else if (lifes == 450)
                    Health = Content.Load<Texture2D>("life_18");
                else if (lifes == 475)
                    Health = Content.Load<Texture2D>("life_19");
                else if (lifes == 500)
                    Health = Content.Load<Texture2D>("life_20");


                for (int i = 0; i < Guard1_Health.Count; i++) {
                    if (Guard1_Health[i] <= 0)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_0");
                    else if (Guard1_Health[i] == 50)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_1");
                    else if (Guard1_Health[i] == 100)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_2");
                    else if (Guard1_Health[i] == 150)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_3");
                    else if (Guard1_Health[i] == 200)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_4");
                    else if (Guard1_Health[i] == 250)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_5");
                    else if (Guard1_Health[i] == 300)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_6");
                    else if (Guard1_Health[i] == 350)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_7");
                    else if (Guard1_Health[i] == 400)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_8");
                    else if (Guard1_Health[i] == 450)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_9");
                    else if (Guard1_Health[i] == 500)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_10");
                    else if (Guard1_Health[i] == 550)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_11");
                    else if (Guard1_Health[i] == 600)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_12");
                    else if (Guard1_Health[i] == 650)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_13");
                    else if (Guard1_Health[i] == 700)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_14");
                    else if (Guard1_Health[i] == 750)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_15");
                    else if (Guard1_Health[i] == 800)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_16");
                    else if (Guard1_Health[i] == 850)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_17");
                    else if (Guard1_Health[i] == 900)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_18");
                    else if (Guard1_Health[i] == 950)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_19");
                    else if (Guard1_Health[i] == 1000)
                        Guard1_health[i].life_eTexture = Content.Load<Texture2D>("life_20");
                }
                if (second_level_simple || second_level_difficult) {
                    for (int i = 0; i < Guard2_Health.Count; i++) {
                        if (Guard2_Health[i] <= 0)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_0");
                        else if (Guard2_Health[i] == 100)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_1");
                        else if (Guard2_Health[i] == 200)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_2");
                        else if (Guard2_Health[i] == 300)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_3");
                        else if (Guard2_Health[i] == 400)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_4");
                        else if (Guard2_Health[i] == 500)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_5");
                        else if (Guard2_Health[i] == 600)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_6");
                        else if (Guard2_Health[i] == 700)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_7");
                        else if (Guard2_Health[i] == 800)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_8");
                        else if (Guard2_Health[i] == 900)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_9");
                        else if (Guard2_Health[i] == 1000)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_10");
                        else if (Guard2_Health[i] == 1100)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_11");
                        else if (Guard2_Health[i] == 1200)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_12");
                        else if (Guard2_Health[i] == 1300)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_13");
                        else if (Guard2_Health[i] == 1400)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_14");
                        else if (Guard2_Health[i] == 1500)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_15");
                        else if (Guard2_Health[i] == 1600)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_16");
                        else if (Guard2_Health[i] == 1700)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_17");
                        else if (Guard2_Health[i] == 1800)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_18");
                        else if (Guard2_Health[i] == 1900)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_19");
                        else if (Guard2_Health[i] == 2000)
                            G2_Health[i].life_eTexture = Content.Load<Texture2D>("life_20");
                    }
                }


                //Вызов процедуры анимации спрайта                        
                ChangeFrame((float)gameTime.ElapsedGameTime.TotalSeconds);
                AnimationGuard1((float)gameTime.ElapsedGameTime.TotalSeconds);
                AnimationGuard2((float)gameTime.ElapsedGameTime.TotalSeconds);

                player.Movement(player);

                foreach (Vertigo_Wall i in Vertigo_Wall) {
                    i.Collide(player, i);
                }

                for (int i = 0; i < Guard1.Count; i++) {
                    Guard1[i].Collide(player, Guard1[i], Guard1_health[i]);
                }

                foreach (Health i in Health_) {
                    i.Collide(player, i);
                }

                foreach (Wall i in wall) {
                    i.Collide(player, i);
                }

                foreach (Container i in Container) {
                    i.Collide(player, i);
                }

                foreach (Key_one i in key) {
                    i.Collide(i, player);
                }

                for (int i = 0; i < G2_Attack.Count; i++) {
                    G2_Attack[i].Move(G2_Attack[i]);
                    if (G2_Attack[i].firePosition.X <= 110f)
                        G2_Attack.RemoveAt(i);
                }

                foreach (Guard2_attack i in G2_Attack) {
                    i.Move(i);
                }

                timeText += gameTime.ElapsedGameTime.TotalSeconds;
                {
                    if (timeText <= 60) {
                        if (first_level_simple || first_level_difficult)
                            Tip = "Up - W, Down - S, Left - A, Right - D\nStrike - Mouse 1";
                        if (second_level_simple || second_level_difficult)
                            Tip = "Up - W, Down - S, Left - A, Right - D\nStrike - Mouse 1";
                    }
                    else Tip = null;
                }

                InvSK = countSilver_keys.ToString();
                InvDK = countKey_door.ToString();
                InvLK = countKey_lattice.ToString();

                base.Update(gameTime);

                foreach (Gate1 i in Gate_1) {
                    for (int j = 0; j < Player_Attack.Count; j++) {
                        if (i.CollideFire_r(i, Player_Attack[j]))
                            Player_Attack.RemoveAt(j);
                    }

                    for (int j = 0; j < Guard1_attack.Count; j++) {
                        if (i.CollideFire_e(i, Guard1_attack[j]))
                            Guard1_attack.RemoveAt(j);
                    }

                    if (i.CollideP(i, player))
                        i.Collide(player, i);
                }

                foreach (Wall i in wall) {
                    for (int j = 0; j < Player_Attack.Count; j++) {
                        if (i.CollideFire_r(i, Player_Attack[j]))
                            Player_Attack.RemoveAt(j);
                    }

                    for (int j = 0; j < Guard1_attack.Count; j++) {
                        if (i.CollideFire_e(i, Guard1_attack[j]))
                            Guard1_attack.RemoveAt(j);
                    }
                }

                for (int i = 0; i < Guard1_attack.Count; i++) {
                    if (Guard1_attack[i].firePosition.X <= 110 || Guard1_attack[i].firePosition.X >= 900)
                        Guard1_attack.RemoveAt(i);
                }

                for (int i = 0; i < Vertigo_Wall.Count; i++) {
                    for (int j = 0; j < Player_Attack.Count; j++) {
                        if (Vertigo_Wall[i].CollideFire_r(Vertigo_Wall[i], Player_Attack[j]))
                            Player_Attack.RemoveAt(j);
                    }

                    for (int j = 0; j < Guard1_attack.Count; j++) {
                        if (Vertigo_Wall[i].CollideFire_e(Vertigo_Wall[i], Guard1_attack[j]))
                            Guard1_attack.RemoveAt(j);
                    }
                }

                for (int i = 0; i < Player_Attack.Count; i++) {
                    if (Player_Attack[i].firePosition.X >= 990)
                        Player_Attack.RemoveAt(i);
                }

                for (int i = 0; i < key.Count; i++) {
                    if (key[i].CollideP(key[i], player)) {
                        text = "Press space to get the key";
                        if (kbState.IsKeyDown(Keys.Space))
                        {
                            get_item.Play();
                            countSilver_keys++;
                            key.RemoveAt(i);
                            text = null;
                        }
                    }
                }


                for (int i = 0; i < Health_.Count; i++) {
                    if (Health_[i].CollideP(player, Health_[i])) {
                        text = "Press space to recover health";
                        if (kbState.IsKeyDown(Keys.Space)) {
                            get_item.Play();
                            lifes += 50;
                            Health_.RemoveAt(i);
                            if (lifes >= 500)
                                lifes = 500;
                            text = null;
                        }
                    }

                }

                for (int i = 0; i < Container.Count; i++) {
                    if (Container[i].CollideP(Container[i], player)) {
                        if (countSilver_keys >= 1) {
                            if (Container_open[i]) {
                                text = "Press space to open container";
                                if (kbState.IsKeyDown(Keys.Space)) {
                                    get_item.Play();
                                    countSilver_keys--;
                                    countKey_lattice++;
                                    text = null;
                                    Container_open[i] = false;
                                }
                            }
                        }
                    }

                }

                for (int i = 0; i < Gate_1.Count; i++) {
                    if (Gate_1[i].CollideP(Gate_1[i], player)) {
                        if (countKey_lattice >= 1) {
                            text = "Press space to open gate";
                            if (kbState.IsKeyDown(Keys.Space)) {
                                get_item.Play();
                                countKey_lattice--;
                                Gate_1.RemoveAt(i);
                                text = null;
                            }
                        }
                    }
                }

                foreach (Sam_attack i in Player_Attack) {
                    i.Move(i);
                }

                if (mState.LeftButton == ButtonState.Pressed) {
                    isPressed++;
                }
                totalTime += gameTime.ElapsedGameTime.TotalSeconds;
                if (totalTime >= 1.0f) {
                    if (isPressed > 0) {
                        Player_Attack.Add(new Sam_attack(Content.Load<Texture2D>("Sam_attack"), new Vector2(player.spPosition.X + 110, player.spPosition.Y + 20)));
                        isPressed = 0;
                        sam_strike.Play();
                    }

                    totalTime = 0;
                }

                elapsedTime += gameTime.ElapsedGameTime.TotalSeconds;
                if (elapsedTime >= 3.0f) {
                    foreach (Guard i in Guard1) {
                        Guard1_attack.Add(new Guard_attack(Content.Load<Texture2D>("Guard_attack"), new Vector2(i.enPosition.X - 55f, i.enPosition.Y + 23f)));
                        guard_strike.Play();
                    }

                    foreach (Guard2 i in Guard_2) {
                        G2_Attack.Add(new Guard2_attack(Content.Load<Texture2D>("Guard2_attack"), new Vector2(i.enPosition.X - 95f, i.enPosition.Y + 20f)));
                        guard_strike.Play();
                    }
                    elapsedTime = 0;
                }

                elapsedTwoTime += gameTime.ElapsedGameTime.TotalSeconds;
                if (elapsedTwoTime >= 2.0f) {
                    foreach (Guard2 i in Guard_2) {
                        G2_Attack.Add(new Guard2_attack(Content.Load<Texture2D>("Guard2_attack"), new Vector2(i.enPosition.X - 95f, i.enPosition.Y + 20f)));
                        guard_strike.Play();
                    }
                    elapsedTwoTime = 0;
                }

                for (int i = 0; i < Guard_2.Count; i++) {
                    if (downEnemy) {
                        Guard_2[i].enPosition.Y += 2;
                        G2_Health[i].life_ePosition.Y += 2;
                    }

                    if (Guard_2[i].enPosition.Y >= 800) {
                        downEnemy = false;
                        upEnemy = true;
                    }
                    if (upEnemy) {
                        Guard_2[i].enPosition.Y -= 2;
                        G2_Health[i].life_ePosition.Y -= 2;
                    }

                    if (Guard_2[i].enPosition.Y <= 350) {
                        downEnemy = true;
                        upEnemy = false;
                    }
                }

                if (second_level_simple || second_level_difficult) {

                    for (int i = 0; i < Guard_2.Count; i++) {
                        if (Guard2_Health[i] <= 0) {
                            Guard_2.RemoveAt(i);
                            Guard2_Health.RemoveAt(i);
                            G2_Health.RemoveAt(i);
                            death.Play();
                        }
                    }
                }

                foreach (Guard_attack i in Guard1_attack) {
                    i.Move(i);
                }

                for (int i = 0; i < Guard1_attack.Count; i++) {
                    if (Guard1_attack[i].Collide(Guard1_attack[i], player)) {
                        Guard1_attack.RemoveAt(i);
                        lifes -= 25;
                        hurt_sound.Play();
                    }
                }

                for (int i = 0; i < G2_Attack.Count; i++) {
                    if (G2_Attack[i].Collide(G2_Attack[i], player)) {
                        G2_Attack.RemoveAt(i);
                        lifes -= 50;
                        hurt_sound.Play();
                    }
                }

                for (int i = 0; i < Gate2_K.Count; i++) {
                    if (Gate2_K[i].Collide(Gate2_K[i], player)) {
                        text = "Press space to take the key";
                        if (kbState.IsKeyDown(Keys.Space)) {
                            get_item.Play();
                            countKey_door++;
                            Gate2_K.RemoveAt(i);
                            text = null;
                        }
                    }
                }

                for (int i = 0; i < Gate1.Count; i++) {
                    if (Gate1[i].Collide(Gate1[i], player)) {
                        if (countKey_door != 0) {
                            text = "Press space to open the door";
                            if (kbState.IsKeyDown(Keys.Space)) {
                                victory = true;
                            }

                        }
                    }
                }

                foreach (Sam_attack i in Player_Attack) {
                    i.Move(i);
                }

                if (first_level_simple || first_level_difficult) {
                    for (int j = 0; j < Guard1.Count; j++) {
                        for (int i = 0; i < Player_Attack.Count; i++) {
                            if (Player_Attack[i].Collide(Player_Attack[i], Guard1[j])) {
                                Player_Attack.RemoveAt(i);
                                Guard1_Health[j] -= 50;
                                hurt_sound.Play();
                            }
                        }
                    }
                }

                for (int i = 0; i < Guard1.Count; i++) {
                    if (Guard1_Health[i] <= 0) {
                        Guard1.RemoveAt(i);
                        Guard1_health.RemoveAt(i);
                        death.Play();
                    }
                }

                for (int i = 0; i < Guard1_Health.Count; i++) {
                    if (Guard1_Health[i] <= 0)
                        Guard1_Health.RemoveAt(i);
                }
                
                if (second_level_simple || second_level_difficult) {
                    for (int i = 0; i < Guard_2.Count; i++) {
                        for (int j = 0; j < Player_Attack.Count; j++) {
                            if (Player_Attack[j].CollideTwo(Player_Attack[j], Guard_2[i])) {
                                hurt_sound.Play();
                                Player_Attack.RemoveAt(j);
                                Guard2_Health[i] -= 50;
                                energy_hit += 100;
                            }
                        }
                    }

                    for (int i = 0; i < Guard1.Count; i++) {
                        for (int j = 0; j < Player_Attack.Count; j++) {
                            if (Player_Attack[j].Collide(Player_Attack[j], Guard1[i])) {
                                hurt_sound.Play();
                                Player_Attack.RemoveAt(j);
                                Guard1_Health[i] -= 50;
                                energy_hit += 50;
                            }
                        }
                    }

                }

                if (lifes <= 0) {
                    defeat = true;
                }
            }

            if (defeat) {
                if (first_level_simple) {
                    if (kbState.IsKeyDown(Keys.Enter)) {
                        unloading();
                        loading = true;
                        defeat = false;
                    }
                }
                if (first_level_difficult) {
                    if (kbState.IsKeyDown(Keys.Enter)) {

                        unloading();
                        loading = true;
                        defeat = false;
                    }
                }
                if (second_level_simple) {
                    if (kbState.IsKeyDown(Keys.Enter)) {
                        unloading();
                        loading = true;
                        defeat = false;
                    }
                }
                if (second_level_difficult) {
                    if (kbState.IsKeyDown(Keys.Enter)) {

                        unloading();
                        loading = true;
                        defeat = false;
                    }
                }
            }

            if (victory) {
                if (second_level_simple || second_level_difficult) {
                    timer2 = (int)timer;
                    timerEnd = timer2 + timer1;
                    if (kbState.IsKeyDown(Keys.Enter)) {
                        mainMenu = true;
                        unloading();
                        second_level_simple = false;
                        second_level_difficult = false;
                        victory = false;
                    }
                    if (writeResults) {
                        MyScore.WriteScore(UserName, timerEnd);
                        writeResults = false;
                    }
                }
                if (first_level_simple) {
                    timer1 = (int)timer;
                                        
                    if (kbState.IsKeyDown(Keys.Enter)) {
                        first_level_simple = false;
                        second_level_simple = true;
                        unloading();
                        load();
                        victory = false;

                    }
                }

                if (first_level_difficult) {
                    timer1 = (int)timer;

                    if (kbState.IsKeyDown(Keys.Enter)) {
                        first_level_difficult = false;
                        second_level_difficult = true;
                        unloading();
                        load();
                        victory = false;
                    }
                }
            }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {

            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();


            if (name) {
                spriteBatch.Draw(nameTexture, namePosition, Color.White);
                spriteBatch.DrawString(MyFontNameTime, UserName, userNamePosition, Color.DeepSkyBlue);
            }


            if (mainMenu) {
                spriteBatch.Draw(mainMenuTexture, mainMenuPosition, Color.White);
            }

            if (difficulty) {
                spriteBatch.Draw(difficultyTexture, difficultyPosition, Color.White);
            }
            if (results) {
                spriteBatch.Draw(resultsTexture, resultsPosition, Color.White);
                ScoreStr = MyScore.ReadScore();
                ScoreStr.Sort(delegate (RecordClass RC1, RecordClass RC2) {
                    return RC1.time.CompareTo(RC2.time);
                });
                int p = 1;
                for (int i = 0; i < ScoreStr.Count; i++) {
                    ScoreStr[i].position = p;
                    p += 1;
                }
                string score = "";
                score += Environment.NewLine;
                if (ScoreStr.Count >= 1)
                score += ScoreStr[0].position.ToString() + " " + ScoreStr[0].name + " " + ScoreStr[0].time.ToString() + Environment.NewLine;
                if (ScoreStr.Count >= 2)
                    score += ScoreStr[1].position.ToString() + " " + ScoreStr[1].name + " " + ScoreStr[1].time.ToString() + Environment.NewLine;
                if (ScoreStr.Count >= 3)
                    score += ScoreStr[2].position.ToString() + " " + ScoreStr[2].name + " " + ScoreStr[2].time.ToString() + Environment.NewLine;
                if (ScoreStr.Count >= 4)
                    score += ScoreStr[3].position.ToString() + " " + ScoreStr[3].name + " " + ScoreStr[3].time.ToString() + Environment.NewLine;
                if (ScoreStr.Count >= 5)
                    score += ScoreStr[4].position.ToString() + " " + ScoreStr[4].name + " " + ScoreStr[4].time.ToString() + Environment.NewLine;
                score += Environment.NewLine + "Real-time result:";
                int j = 0;
                for (int i = 0; i < ScoreStr.Count; i++) {
                    if ((ScoreStr[i].name == UserName) && (ScoreStr[i].time == timerEnd)) {
                        j = ScoreStr[i].position;
                    }
                }
                score += Environment.NewLine;
                score += j.ToString() + " ";
                score += UserName;
                score += " ";
                score += timerEnd;

                Vector2 ScrStr = new Vector2(123f, 140f);
                spriteBatch.DrawString(MyFontNameTime, score, ScrStr, Color.DeepSkyBlue);
            }

            if ((first_level_simple || first_level_difficult || second_level_simple || second_level_difficult)) {
                spriteBatch.Draw(Grid_Map, position_map_first, Color.White);
                spriteBatch.Draw(player.spTexture, player.spPosition, sprRec, Color.White);
                foreach (Sam_attack i in Player_Attack) {
                    spriteBatch.Draw(i.fireTexture, i.firePosition, Color.White);
                }

                foreach (Guard_attack i in Guard1_attack) {
                    spriteBatch.Draw(i.fireTexture, i.firePosition, Color.White);
                }


                foreach (Health i in Health_) {
                    spriteBatch.Draw(i.lifeTexture, i.lifePosition, Color.White);
                }

                foreach (Container i in Container) {
                    spriteBatch.Draw(i.box_oneTexture, i.box_onePosition, Color.White);
                }

                foreach (Key_one i in key) {
                    spriteBatch.Draw(i.keyTexture, i.keyPosition, Color.White);
                }

                foreach (Wall i in wall) {
                    spriteBatch.Draw(i.wallTexture, i.wallPosition, Color.White);
                }

                foreach (Gate2 i in Gate1) {
                    spriteBatch.Draw(i.doorTexture, i.doorPosition, Color.White);
                }

                foreach (Wall i in wall) {
                    spriteBatch.Draw(i.wallTexture, i.wallPosition, Color.White);
                }

                foreach (Guard i in Guard1) {
                    spriteBatch.Draw(i.enTexture, i.enPosition, Rec, Color.White);
                }

                foreach (Vertigo_Wall i in Vertigo_Wall) {
                    spriteBatch.Draw(i.wallTexture, i.wallPosition, Color.White);
                }

                foreach (Gate1 i in Gate_1) {
                    spriteBatch.Draw(i.latTexture, i.latPosition, Color.White);
                }

                foreach (Gate2_key i in Gate2_K) {
                    spriteBatch.Draw(i.dkTexture, i.dkPosition, Color.White);
                }

                spriteBatch.Draw(panel, posPanel, Color.White);
                if (Tip != null) spriteBatch.DrawString(MyFontText, Tip, posTip, Color.White);

                spriteBatch.Draw(Container_key, posInSK, Color.White);
                spriteBatch.Draw(Gate2_key, posInDK, Color.White);
                spriteBatch.Draw(Gate1_key, posInLK, Color.White);
                spriteBatch.DrawString(MyFontText, InvSK, posInTextSK, Color.White);
                spriteBatch.DrawString(MyFontText, InvDK, posInTextDK, Color.White);
                spriteBatch.DrawString(MyFontText, InvLK, posInTextLK, Color.White);
                if (text != null) spriteBatch.DrawString(MyFontText, text, posText, Color.White);

                spriteBatch.Draw(Health, posInLR, Color.White);

                foreach (Guard_Health i in Guard1_health) {
                    spriteBatch.Draw(i.life_eTexture, i.life_ePosition, Color.White);
                }
                foreach (Guard2 i in Guard_2) {
                    spriteBatch.Draw(i.enTexture, i.enPosition, Recc, Color.White);
                }
                foreach (Guard2_Health i in G2_Health) {
                    spriteBatch.Draw(i.life_eTexture, i.life_ePosition, Color.White);
                }
                foreach (Guard2_attack i in G2_Attack) {
                    spriteBatch.Draw(i.fireTexture, i.firePosition, Color.White);
                }

                if (first_level_simple || first_level_difficult) {
                    TimeFirst = "Time: " + timer1.ToString();
                    spriteBatch.DrawString(MyFontNameTime, TimeFirst, TimeFirstPos, Color.DeepSkyBlue);
                }
                if (second_level_simple || second_level_difficult) {
                    TimeFirst = "Time: " + timer2.ToString();
                    spriteBatch.DrawString(MyFontNameTime, TimeFirst, TimeFirstPos, Color.DeepSkyBlue);
                }

            }

            if (defeat) {
                spriteBatch.Draw(defeatTexture, defeatPosition, Color.White);
            }
            if (victory) {
                spriteBatch.Draw(victoryTexture, victoryPosition, Color.White);
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}