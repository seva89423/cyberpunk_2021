﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Tr2n_Game
{
    public class Health
    {
        public Texture2D lifeTexture;
        public Vector2 lifePosition;

        public Health(Texture2D newLifeTexture, Vector2 newLifePosition)
        {
            lifeTexture = newLifeTexture;
            lifePosition.X = newLifePosition.X * 110 + 35;
            lifePosition.Y = newLifePosition.Y * 100 + 20;
        }

        public bool CollideP(Sam_Flynn mySpriteObj, Health life)
        {
            if (mySpriteObj.spPosition.X + 111 >= life.lifePosition.X &&
                mySpriteObj.spPosition.X <= life.lifePosition.X + 34 &&
                mySpriteObj.spPosition.Y + 101 >= life.lifePosition.Y &&
                mySpriteObj.spPosition.Y <= life.lifePosition.Y + 53)
                return true;
            else return false;

        }

        public bool CollideMoreLess(Sam_Flynn mySpriteObj, Health life)
        {
            if (mySpriteObj.spPosition.X + 110 >= life.lifePosition.X &&
                mySpriteObj.spPosition.X <= life.lifePosition.X + 33 &&
                mySpriteObj.spPosition.Y + 100 >= life.lifePosition.Y &&
                mySpriteObj.spPosition.Y <= life.lifePosition.Y + 52)
                return true;
            else return false;

        }

        public void Collide(Sam_Flynn mySpriteObj, Health life)
        {
            KeyboardState kbState = Keyboard.GetState();
            if (kbState.IsKeyDown(Keys.W))
            {
                if (life.CollideMoreLess(mySpriteObj, life))
                    mySpriteObj.spPosition.Y = life.lifePosition.Y + 53;
            }

            if (kbState.IsKeyDown(Keys.S))
            {
                if (life.CollideMoreLess(mySpriteObj, life))
                    mySpriteObj.spPosition.Y = life.lifePosition.Y - 101;
            }

            if (kbState.IsKeyDown(Keys.A))
            {
                if (life.CollideMoreLess(mySpriteObj, life))
                    mySpriteObj.spPosition.X = life.lifePosition.X + 34;
            }

            if (kbState.IsKeyDown(Keys.D))
            {
                if (life.CollideMoreLess(mySpriteObj, life))
                    mySpriteObj.spPosition.X = life.lifePosition.X - 111;
            }
        }
    }
}