﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Tr2n_Game
{
    public class Container
    {
        public Texture2D box_oneTexture;
        public Vector2 box_onePosition;

        public Container(Texture2D newbox_oneTexture, Vector2 newbox_onePosition)
        {
            box_oneTexture = newbox_oneTexture;
            box_onePosition.X = newbox_onePosition.X * 110;
            box_onePosition.Y = newbox_onePosition.Y * 100 + 5;
        }

        public bool CollideP(Container box, Sam_Flynn mySpriteObj)
        {
            if (mySpriteObj.spPosition.X <= box.box_onePosition.X + 101 &&
               mySpriteObj.spPosition.X + 111 >= box.box_onePosition.X &&
               mySpriteObj.spPosition.Y <= box.box_onePosition.Y + 86 &&
               mySpriteObj.spPosition.Y + 101 >= box.box_onePosition.Y)
                return true;
            else return false;
        }

        public bool CollideMoreLess(Container box, Sam_Flynn mySpriteObj)
        {
            if (mySpriteObj.spPosition.X < box.box_onePosition.X + 100 &&
               mySpriteObj.spPosition.X + 110 > box.box_onePosition.X &&
               mySpriteObj.spPosition.Y < box.box_onePosition.Y + 85 &&
               mySpriteObj.spPosition.Y + 100 > box.box_onePosition.Y)
                return true;
            else return false;
        }

        public void Collide(Sam_Flynn mySpriteObj, Container box)
        {
            KeyboardState kbState = Keyboard.GetState();
            if (kbState.IsKeyDown(Keys.W))
            {
                if (box.CollideMoreLess(box, mySpriteObj))
                    mySpriteObj.spPosition.Y = box.box_onePosition.Y + 86;
            }

            if (kbState.IsKeyDown(Keys.S))
            {
                if (box.CollideMoreLess(box, mySpriteObj))
                    mySpriteObj.spPosition.Y = box.box_onePosition.Y - 86;
            }

            if (kbState.IsKeyDown(Keys.D))
            {
                if (box.CollideMoreLess(box, mySpriteObj))
                    mySpriteObj.spPosition.X = box.box_onePosition.X - 101;
            }

            if (kbState.IsKeyDown(Keys.A))
            {
                if (box.CollideMoreLess(box, mySpriteObj))
                    mySpriteObj.spPosition.X = box.box_onePosition.X + 101;
            }
        }
    }
}