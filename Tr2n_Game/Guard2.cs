﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Tr2n_Game
{
    public class Guard2 : Guard
    {
        Texture2D EnemyTexture;
        Vector2 EnemyPosition;

        public Guard2(Texture2D newEnemyTexture, Vector2 newEnemyPosition) : base(newEnemyTexture, newEnemyPosition)
        {
            EnemyTexture = newEnemyTexture;
            EnemyPosition.X = newEnemyPosition.X * 110;
            EnemyPosition.Y = newEnemyPosition.Y * 100;
        }

        public void Move(Guard2 enemy_two)
        {
            enemy_two.EnemyPosition.X += 5;
        }
    }
}