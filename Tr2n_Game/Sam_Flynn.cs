﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Tr2n_Game
{
    public class Sam_Flynn
    {

        public Texture2D spTexture;
        public Vector2 spPosition;
        int spriteWidth = 110;
        int spriteHeight = 100;
        public int isPressed = 0;
        public Sam_Flynn(Texture2D newSpTexture, Vector2 newSpPosition)
        {
            spTexture = newSpTexture;
            spPosition = newSpPosition;
        }

        public void Movement(Sam_Flynn mySpriteObj)
        {
            KeyboardState kbState = Keyboard.GetState();
            if (kbState.IsKeyDown(Keys.W))
            {
                if (mySpriteObj.spPosition.Y <= 100)
                    mySpriteObj.spPosition.Y = 100;
                else mySpriteObj.spPosition.Y -= 5;
            }

            else if (kbState.IsKeyDown(Keys.S))
            {
                if (mySpriteObj.spPosition.Y >= (900 - spriteHeight))
                    mySpriteObj.spPosition.Y = 900 - spriteHeight;
                else mySpriteObj.spPosition.Y += 5;
            }

            else if (kbState.IsKeyDown(Keys.A))
            {
                if (mySpriteObj.spPosition.X <= 110)
                    mySpriteObj.spPosition.X = 110;
                else mySpriteObj.spPosition.X -= 5;
            }
            else if (kbState.IsKeyDown(Keys.D))
            {
                if (mySpriteObj.spPosition.X >= (1045 - spriteWidth))
                    mySpriteObj.spPosition.X = 1045 - spriteWidth;
                else mySpriteObj.spPosition.X += 5;
            }
        }
    }
}