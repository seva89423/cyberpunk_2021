﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Tr2n_Game
{
    public class Guard2_attack
    {
        public Texture2D fireTexture;
        public Vector2 firePosition;

        public Guard2_attack(Texture2D newFireTexture, Vector2 newFirePosition)
        {
            fireTexture = newFireTexture;
            firePosition = newFirePosition;
        }

        public void Move(Guard2_attack fire_en_two)
        {
            fire_en_two.firePosition.X -= 5;
        }
        public bool Collide(Guard2_attack fire_e_two, Sam_Flynn mySpriteObj)
        {
            if (fire_e_two.firePosition.X <= mySpriteObj.spPosition.X + 100 &&
               fire_e_two.firePosition.X + 85 >= mySpriteObj.spPosition.X &&
               fire_e_two.firePosition.Y <= mySpriteObj.spPosition.Y + 80 &&
               fire_e_two.firePosition.Y + 45 >= mySpriteObj.spPosition.Y)
                return true;
            else return false;
        }
    }
}