﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Tr2n_Game
{
    public class Guard
    {
        public Texture2D enTexture;
        public Vector2 enPosition;
        
        public Guard(Texture2D newEnTexture, Vector2 newEnPosition)
        {
            enTexture = newEnTexture;
            enPosition.X = newEnPosition.X * 110 + 2;
            enPosition.Y = newEnPosition.Y * 100;
        }

        public bool CollideMoreLess(Sam_Flynn mySpriteObj, Guard enemy, Guard_Health life_e)
        {
            if (mySpriteObj.spPosition.X < enemy.enPosition.X + 100 &&
               mySpriteObj.spPosition.X + 110 > enemy.enPosition.X &&
               mySpriteObj.spPosition.Y < enemy.enPosition.Y + 100 &&
               mySpriteObj.spPosition.Y + 110 > life_e.life_ePosition.Y)
                return true;
            else return false;
        }

        public void Collide(Sam_Flynn mySpriteObj, Guard enemy, Guard_Health life_e)
        {
            KeyboardState kbState = Keyboard.GetState();
            if (kbState.IsKeyDown(Keys.W))
            {
                if (enemy.CollideMoreLess(mySpriteObj, enemy, life_e))
                    mySpriteObj.spPosition.Y = enemy.enPosition.Y + 101;
            }

            if (kbState.IsKeyDown(Keys.S))
            {
                if (enemy.CollideMoreLess(mySpriteObj, enemy, life_e))
                    mySpriteObj.spPosition.Y = life_e.life_ePosition.Y - 105;
            }

            if (kbState.IsKeyDown(Keys.D))
            {
                if (enemy.CollideMoreLess(mySpriteObj, enemy, life_e))
                    mySpriteObj.spPosition.X = enemy.enPosition.X - 111;
            }

            if (kbState.IsKeyDown(Keys.A))
            {
                if (enemy.CollideMoreLess(mySpriteObj, enemy, life_e))
                    mySpriteObj.spPosition.X = enemy.enPosition.X + 111;
            }
        }
    }
}