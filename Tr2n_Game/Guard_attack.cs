﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Tr2n_Game
{
    public class Guard_attack
    {
        public Texture2D fireTexture;
        public Vector2 firePosition;

        public Guard_attack(Texture2D newFireTexture, Vector2 newFirePosition)
        {
            fireTexture = newFireTexture;
            firePosition = newFirePosition;
        }

        public void Move(Guard_attack fire_e)
        {
            fire_e.firePosition.X -= 5;
        }
        public bool Collide(Guard_attack fire_e, Sam_Flynn mySpriteObj)
        {
            if (fire_e.firePosition.X <= mySpriteObj.spPosition.X + 55 &&
               fire_e.firePosition.X + 55 >= mySpriteObj.spPosition.X &&
               fire_e.firePosition.Y <= mySpriteObj.spPosition.Y + 50 &&
               fire_e.firePosition.Y + 50 >= mySpriteObj.spPosition.Y)
                return true;
            else return false;
        }
    }
}
