﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Tr2n_Game
{
    public class Sam_attack
    {
        public Texture2D fireTexture;
        public Vector2 firePosition;

        

        public Sam_attack(Texture2D newFireTexture, Vector2 newFirePosition)
        {
            fireTexture = newFireTexture;
            firePosition = newFirePosition;
            
        }

        public void Move(Sam_attack fire_r)
        {
            firePosition.X += 4;
        }

        public bool Collide(Sam_attack fire_r, Guard enemy)
        {
            if (fire_r.firePosition.X <= enemy.enPosition.X + 110 &&
               fire_r.firePosition.X + 55 >= enemy.enPosition.X &&
               fire_r.firePosition.Y <= enemy.enPosition.Y + 100 &&
               fire_r.firePosition.Y + 50 >= enemy.enPosition.Y)
                return true;
            else return false;
        }

        public bool CollideTwo(Sam_attack fire_r, Guard2 enemy_two)
        {
            if (fire_r.firePosition.X <= enemy_two.enPosition.X + 110 &&
               fire_r.firePosition.X + 55 >= enemy_two.enPosition.X &&
               fire_r.firePosition.Y <= enemy_two.enPosition.Y + 100 &&
               fire_r.firePosition.Y + 50 >= enemy_two.enPosition.Y)
                return true;
            else return false;
        }
    }
}