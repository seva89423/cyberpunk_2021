﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Tr2n_Game
{
    public class Guard2_Health
    {
        public Texture2D life_eTexture;
        public Vector2 life_ePosition;

        public Guard2_Health(Texture2D newLife_eTexture, Vector2 newLife_ePosition)
        {
            life_eTexture = newLife_eTexture;
            life_ePosition = newLife_ePosition;
        }
    }
}