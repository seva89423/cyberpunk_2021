﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Tr2n_Game
{
    public class Gate2_key
    {
        public Texture2D dkTexture;
        public Vector2 dkPosition;

        public Gate2_key(Texture2D newDkTexture, Vector2 newDkPosition)
        {
            dkTexture = newDkTexture;
            dkPosition.X = newDkPosition.X * 110 + 22;
            dkPosition.Y = newDkPosition.Y * 100 + 20;
        }

        public bool Collide(Gate2_key door_k, Sam_Flynn mySpriteObj)
        {
            if (mySpriteObj.spPosition.X <= door_k.dkPosition.X + 55 &&
               mySpriteObj.spPosition.X + 110 >= door_k.dkPosition.X &&
               mySpriteObj.spPosition.Y <= door_k.dkPosition.Y + 50 &&
               mySpriteObj.spPosition.Y + 100 >= door_k.dkPosition.Y)
                return true;
            return false;
        }
    }
}