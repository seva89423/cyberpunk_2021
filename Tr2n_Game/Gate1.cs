﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Tr2n_Game
{
    public class Gate1
    {
        public Texture2D latTexture;
        public Vector2 latPosition;

        public Gate1(Texture2D newLatTexture, Vector2 newLatPosition)
        {
            latTexture = newLatTexture;
            latPosition.X = newLatPosition.X * 110 + 110;
            latPosition.Y = newLatPosition.Y * 100;
        }

        public bool CollideP(Gate1 lat, Sam_Flynn mySpriteObj)
        {
            if (mySpriteObj.spPosition.X <= lat.latPosition.X + 110 &&
               mySpriteObj.spPosition.X + 110 >= lat.latPosition.X &&
               mySpriteObj.spPosition.Y <= lat.latPosition.Y + 20 &&
               mySpriteObj.spPosition.Y + 20 >= lat.latPosition.Y)
                return true;
            return false;
        }



        public bool CollideFire_r(Gate1 lat, Sam_attack fire_r)
        {
            if (fire_r.firePosition.X <= lat.latPosition.X + 55 &&
               fire_r.firePosition.X + 80 >= lat.latPosition.X &&
               fire_r.firePosition.Y <= lat.latPosition.Y + 100 &&
               fire_r.firePosition.Y + 50 >= lat.latPosition.Y)
                return true;
            return false;
        }

        public bool CollideFire_e(Gate1 lat, Guard_attack fire_e)
        {
            if (fire_e.firePosition.X <= lat.latPosition.X + 55 &&
               fire_e.firePosition.X + 55 >= lat.latPosition.X &&
               fire_e.firePosition.Y <= lat.latPosition.Y + 100 &&
               fire_e.firePosition.Y + 100 >= lat.latPosition.Y)
                return true;
            return false;
        }

        public bool CollideMoreLess(Sam_Flynn mySpriteObj, Gate1 lat)
        {
            if (mySpriteObj.spPosition.X + 110 > lat.latPosition.X &&
                mySpriteObj.spPosition.X < lat.latPosition.X + 55 &&
                mySpriteObj.spPosition.Y + 100 > lat.latPosition.Y &&
                mySpriteObj.spPosition.Y < lat.latPosition.Y + 100)
                return true;
            else return false;
        }

        public void Collide(Sam_Flynn mySpriteObj, Gate1 lat)
        {
            KeyboardState kbState = Keyboard.GetState();
            if (kbState.IsKeyDown(Keys.D))
            {
                if (lat.CollideMoreLess(mySpriteObj, lat))
                    mySpriteObj.spPosition.X = lat.latPosition.X - 110;
            }

            else if (kbState.IsKeyDown(Keys.A))
            {
                if (lat.CollideMoreLess(mySpriteObj, lat))
                    mySpriteObj.spPosition.X = lat.latPosition.X + 55;
            }
        }
    }
}