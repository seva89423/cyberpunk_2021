﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Tr2n_Game
{
    public class Key_one
    {
        public Texture2D keyTexture;
        public Vector2 keyPosition;

        public Key_one(Texture2D newKeyTexture, Vector2 newKeyPosition)
        {
            keyTexture = newKeyTexture;
            keyPosition.X = newKeyPosition.X * 110 + 10;
            keyPosition.Y = newKeyPosition.Y * 100 + 20;

        }
        public bool CollideP(Key_one silver_keys, Sam_Flynn mySpriteObj)
        {
            if (mySpriteObj.spPosition.X <= silver_keys.keyPosition.X + 41 &&
               mySpriteObj.spPosition.X + 111 >= silver_keys.keyPosition.X &&
               mySpriteObj.spPosition.Y <= silver_keys.keyPosition.Y + 51 &&
               mySpriteObj.spPosition.Y + 101>= silver_keys.keyPosition.Y)
                return true;
            else return false;
        }

        public bool CollideMoreLess(Key_one silver_keys, Sam_Flynn mySpriteObj)
        {
            if (mySpriteObj.spPosition.X <= silver_keys.keyPosition.X + 40 &&
               mySpriteObj.spPosition.X + 110 >= silver_keys.keyPosition.X &&
               mySpriteObj.spPosition.Y <= silver_keys.keyPosition.Y + 50 &&
               mySpriteObj.spPosition.Y + 100 >= silver_keys.keyPosition.Y)
                return true;
            else return false;
        }

        public void Collide(Key_one silver_keys, Sam_Flynn mySpriteObj)
        {
            KeyboardState kbState = Keyboard.GetState();
            if (kbState.IsKeyDown(Keys.W))
            {
                if (silver_keys.CollideMoreLess(silver_keys ,mySpriteObj))
                    mySpriteObj.spPosition.Y = silver_keys.keyPosition.Y + 51;
            }

            if (kbState.IsKeyDown(Keys.S))
            {
                if (silver_keys.CollideMoreLess(silver_keys, mySpriteObj))
                    mySpriteObj.spPosition.Y = silver_keys.keyPosition.Y - 101;
            }

            if (kbState.IsKeyDown(Keys.D))
            {
                if (silver_keys.CollideMoreLess(silver_keys, mySpriteObj))
                    mySpriteObj.spPosition.X = silver_keys.keyPosition.X - 111;
            }

            if (kbState.IsKeyDown(Keys.A))
            {
                if (silver_keys.CollideMoreLess(silver_keys, mySpriteObj))
                    mySpriteObj.spPosition.X = silver_keys.keyPosition.X + 41;
            }
        }
    }
}