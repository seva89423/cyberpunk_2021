﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Tr2n_Game
{
    public class Gate2
    {
        public Texture2D doorTexture;
        public Vector2 doorPosition;

        public Gate2(Texture2D newDoorTexture, Vector2 newDoorPosition)
        {
            doorTexture = newDoorTexture;
            doorPosition.X = newDoorPosition.X * 110 + 55;
            doorPosition.Y = newDoorPosition.Y * 100;
        }

        public bool Collide(Gate2 door, Sam_Flynn mySpriteObj)
        {
            if (mySpriteObj.spPosition.X <= door.doorPosition.X + 55 &&
               mySpriteObj.spPosition.X + 110 >= door.doorPosition.X &&
               mySpriteObj.spPosition.Y <= door.doorPosition.Y + 10 &&
               mySpriteObj.spPosition.Y + 10 >= door.doorPosition.Y)
                return true;
            return false;
        }
    }
}