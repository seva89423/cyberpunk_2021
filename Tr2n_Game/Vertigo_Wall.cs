﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Tr2n_Game
{
    public class Vertigo_Wall
    {
        public Texture2D wallTexture;
        public Vector2 wallPosition;

        public Vertigo_Wall(Texture2D newWallTexture, Vector2 newWallPosition)
        {
            wallTexture = newWallTexture;
            wallPosition.X = newWallPosition.X * 110 + 110;
            wallPosition.Y = newWallPosition.Y * 100;
        }

        public bool CollideFire_r(Vertigo_Wall wall_v, Sam_attack fire_r)
        {
            if (fire_r.firePosition.X <= wall_v.wallPosition.X + 45 &&
               fire_r.firePosition.X + 80 >= wall_v.wallPosition.X &&
               fire_r.firePosition.Y <= wall_v.wallPosition.Y + 95 &&
               fire_r.firePosition.Y + 50 >= wall_v.wallPosition.Y)
                return true;
            else return false;
        }

        public bool CollideFire_e(Vertigo_Wall wall_v, Guard_attack fire_e)
        {
            if (fire_e.firePosition.X <= wall_v.wallPosition.X + 45 &&
               fire_e.firePosition.X + 55 >= wall_v.wallPosition.X &&
               fire_e.firePosition.Y <= wall_v.wallPosition.Y + 95 &&
               fire_e.firePosition.Y + 50 >= wall_v.wallPosition.Y)
                return true;
            else return false;
        }

        public bool CollideFire_en_two(Vertigo_Wall wall_v, Guard2_attack fire_en_two)
        {
            if (fire_en_two.firePosition.X <= wall_v.wallPosition.X + 45 &&
               fire_en_two.firePosition.X + 95 >= wall_v.wallPosition.X &&
               fire_en_two.firePosition.Y <= wall_v.wallPosition.Y + 95 &&
               fire_en_two.firePosition.Y + 70 >= wall_v.wallPosition.Y)
                return true;
            else return false;
        }

        public bool CollideMoreLess(Vertigo_Wall wall_v, Sam_Flynn mySpriteObj)
        {
            if (mySpriteObj.spPosition.X < wall_v.wallPosition.X + 45 &&
                mySpriteObj.spPosition.X + 110 > wall_v.wallPosition.X &&
                mySpriteObj.spPosition.Y < wall_v.wallPosition.Y + 90 &&
                mySpriteObj.spPosition.Y + 90 > wall_v.wallPosition.Y)
                return true;
            else return false;
        }
        public void Collide(Sam_Flynn mySpriteObj, Vertigo_Wall wall_v)
        {
            KeyboardState kbState = Keyboard.GetState();
            if (kbState.IsKeyDown(Keys.W))
            {
                if (wall_v.CollideMoreLess(wall_v, mySpriteObj))
                    mySpriteObj.spPosition.Y = wall_v.wallPosition.Y + 90;
            }

            else if (kbState.IsKeyDown(Keys.S))
            {
                if (wall_v.CollideMoreLess(wall_v, mySpriteObj))
                    mySpriteObj.spPosition.Y = wall_v.wallPosition.Y - 90;
            }

            else if (kbState.IsKeyDown(Keys.D))
            {
                if (wall_v.CollideMoreLess(wall_v, mySpriteObj))
                    mySpriteObj.spPosition.X = wall_v.wallPosition.X - 110;
            }

            else if (kbState.IsKeyDown(Keys.A))
            {
                if (wall_v.CollideMoreLess(wall_v, mySpriteObj))
                    mySpriteObj.spPosition.X = wall_v.wallPosition.X + 45;
            }
        }
    }
}