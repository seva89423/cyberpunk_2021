﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Tr2n_Game
{
    public class Wall
    {
        public Texture2D wallTexture;
        public Vector2 wallPosition;
        public Wall(Texture2D newWallTexture, Vector2 newWallPosition)
        {
            wallTexture = newWallTexture;
            wallPosition.X = newWallPosition.X * 110;
            wallPosition.Y = newWallPosition.Y * 100;
        }

        public bool CollideMoreLess(Sam_Flynn mySpriteObj, Wall wall)
        {
            if (mySpriteObj.spPosition.X + 110 > wall.wallPosition.X &&
                mySpriteObj.spPosition.X < wall.wallPosition.X + 110 &&
                mySpriteObj.spPosition.Y + 100 > wall.wallPosition.Y &&
                mySpriteObj.spPosition.Y < wall.wallPosition.Y + 50)
                return true;
            else return false;
        }

        

        public bool CollideFire_r(Wall wall, Sam_attack fire_r)
        {
            if (fire_r.firePosition.X <= wall.wallPosition.X + 110 &&
               fire_r.firePosition.X + 80 >= wall.wallPosition.X &&
               fire_r.firePosition.Y <= wall.wallPosition.Y + 50 &&
               fire_r.firePosition.Y + 50 >= wall.wallPosition.Y)
                return true;
            else return false;
        }

        public bool CollideFire_e(Wall wall, Guard_attack fire_e)
        {
            if (fire_e.firePosition.X <= wall.wallPosition.X + 100 &&
               fire_e.firePosition.X + 55 >= wall.wallPosition.X &&
               fire_e.firePosition.Y <= wall.wallPosition.Y + 50 &&
               fire_e.firePosition.Y + 50 >= wall.wallPosition.Y)
                return true;
            else return false;
        }

        public void Collide(Sam_Flynn mySpriteObj, Wall wall)
        {
            KeyboardState kbState = Keyboard.GetState();
            if (kbState.IsKeyDown(Keys.W))
            {
                if (wall.CollideMoreLess(mySpriteObj, wall))
                    mySpriteObj.spPosition.Y = wall.wallPosition.Y + 50;
            }

            if (kbState.IsKeyDown(Keys.S))
            {
                if (wall.CollideMoreLess(mySpriteObj, wall))
                    mySpriteObj.spPosition.Y = wall.wallPosition.Y - 100;
            }

            if (kbState.IsKeyDown(Keys.D))
            {
                if (wall.CollideMoreLess(mySpriteObj, wall))
                    mySpriteObj.spPosition.X = wall.wallPosition.X - 110;
            }

            if (kbState.IsKeyDown(Keys.A))
            {
                if (wall.CollideMoreLess(mySpriteObj, wall))
                    mySpriteObj.spPosition.X = wall.wallPosition.X + 110;
            }
        }
    }
}